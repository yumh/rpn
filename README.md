# RPN

A simple calculator for the reverse polish notation written before I discovered
`dc(1)`.

To compile
```sh
$ cc rpn.c -o rpn -lm
```

To read the manpage
```sh
$ mandoc -l rpn.1
```
