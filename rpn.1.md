RPN(1) - General Commands Manual

# NAME

**rpn** - simple reverse polish notation calculator

# SYNOPSIS

**rpn**
\[**-h**]
\[**-d**&nbsp;*a=num*]
\[**-F**&nbsp;*file*]
\[**-f**&nbsp;*file*]
\[**-p**&nbsp;*prompt*]

# DESCRIPTION

**rpn**
is a simple reverse polish notation calculator. It can source multiple files,
has built-in support for variables and various operations.

All option are performed in the given order so, for example

	rpn -dz=4 -f one -F two -f -

will first initialize the variable
*z*
to the value 4, then execute the file
*one*, then clear the stack and reset the variables, executes the file
*two*
and finally starts reading from
`stdin`.

Variables are restricted to a single lower or uppercase letter, all numbers are
stored internally as
*long double*.

# SPECIAL VARIABLES

At the startup, or after a clean-up with
**-F**
the following variables will be defined:

e

> Euler's number.

p

> Archimedes' constant (pi).

# OPTIONS

**-h**

> Print a small usage message to
> `stderr`
> and then exit.

**-d** *a=num*

> Set the variable
> *a*
> to the value num. The equal sign can be omitted.

**-F** *file*

> Clear both the stack and the variables before executing the file. The special
> value
> `-`
> means
> `stdin`.

**-f** *file*

> Execute the file without wiping the stack or the variables,
> `-`
> means
> `stdin`.

**-p** *prompt*

> Override the prompt with
> *prompt*.

# OPERATIONS

*+ - \* ^*

> The plus, minus, times and power operator. They pop from the stack two elements
> and push into the stack the result of the operation.

*/ %*

> Division and remainder.

*exp*

> Pops out of the stack a value and push e at the power of that value.

*log2*

> Pops out of the stack a value and push the log in base 2 of that value.

*ln*

> Similar to log2 but uses the Euler's constant as base.

*p*

> Peek operation. Print the top of the stack.

*P*

> Pop operation. Pop a value from the stack and print it.

*debug*

> Dumps the stack, useful for debugging.

*q*

> Quit.

*sa*

> Peeks a value from the stack and save it as the variable
> *a*.

*Sa*

> Pops a value from the stack and save it as the variable
> *a*.

*=a*

> Push into the stack the value of the variable
> *a*.

Any other command will be silently ignored.

# ENVIRONMENT

`PS2`

> If reading input from
> `stdin`
> the content of the
> `PS2`,
> or "&gt; " if
> `NULL`, will printed at each newline. This value can be overrided with
> the
> **-p**
> option.

OpenBSD 6.3 - August 10, 2018
