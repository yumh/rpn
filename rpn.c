#define __POSIX_VISIBLE 201403

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sysexits.h>
#include <errno.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <unistd.h>

#define STACK_START_SIZE 16

struct stack {
	size_t cap;
	size_t size;
	long double *stack;
};

struct stack *
create_stack(size_t capacity)
{
	struct stack *s = malloc(sizeof(struct stack));
	if (s == NULL)
		return s;

	s->cap = capacity;
	s->size = 0;
	s->stack = calloc(capacity, sizeof(long double));
	if (s->stack == NULL) {
		free(s);
		return NULL;
	}

	return s;
}

void
destroy_stack(struct stack *s)
{
	free(s->stack);
	free(s);
}

bool
push_stack(struct stack *s, long double d)
{
	/* resize if needed */
	if (s->size == s->cap) {
		s->cap += s->cap >>1;
		long double *b = realloc(s->stack, s->cap);
		if (b == NULL)
			return false;
		s->stack = b;

		/* make sure the stack is filled with 0s */
		/* OpenBSD' recallocarray would be handy, but not portable */
		for (size_t i = s->size +1; i < s->cap; i++)
			s->stack[i] = 0;
	}

	s->stack[s->size] = d;
	s->size++;
	return true;
}

long double
pop_stack(struct stack *s)
{
	if (s->size == 0)
		return 0;

	s->size--;
	long double ret = s->stack[s->size];
	s->stack[s->size] = 0;
	return ret;
}

long double
peek_stack(struct stack *s)
{
	if (s->size == 0)
		return 0;
	return s->stack[s->size - 1];
}

void
usage(char *prgname)
{
	fprintf(stderr, "Usage: %s [-h] [-d a=num] [-F file] [-f file] [-p prompt]\n", prgname);
}

bool
exec_file(FILE *f, long double *vars, char *prompt, struct stack *s)
{
	size_t read = 0;
	size_t len = 16;
	char *buf = calloc(len, sizeof(char));
	if (buf == NULL) {
		fprintf(stderr, "Could not allocate memory!\n");
		return false;
	}

	if (f == stdin)
		fprintf(stderr, "%s", prompt);

	while (true) {
		int c = fgetc(f);
		if (c == '\n' && strlen(buf) == 0 && f == stdin)
			fprintf(stderr, "%s", prompt);

		if (isspace(c) && strlen(buf) == 0)
			continue;

		if (isspace(c) || c == EOF) {
			char *end = NULL;
			errno = 0;
			long double o = strtold(buf, &end);
			if (errno != 0 || end == buf) {
				/* maybe it's a function! */

				if (!strcmp(buf, "+")) {		/* add */
					long double a = pop_stack(s);
					long double b = pop_stack(s);
					push_stack(s, a+b);
				} else if (!strcmp(buf, "-")) {		/* sub */
					long double a = pop_stack(s);
					long double b = pop_stack(s);
					push_stack(s, b-a);
				} else if (!strcmp(buf, "*")) {		/* mul */
					long double a = pop_stack(s);
					long double b = pop_stack(s);
					push_stack(s, a*b);
				} else if (!strcmp(buf, "/")) {		/* div */
					long double a = pop_stack(s);
					long double b = pop_stack(s);
					push_stack(s, b/a);
				} else if (!strcmp(buf, "%")) {		/* rem */
					long double a = pop_stack(s);
					long double b = pop_stack(s);
					push_stack(s, (long)b % (long)a);
				} else if (!strcmp(buf, "^")) {		/* pow */
					long double a = pop_stack(s);
					long double b = pop_stack(s);
					push_stack(s, powl(b, a));
				} else if (!strcmp(buf, "exp")) {	/* exp */
					long double a = pop_stack(s);
					push_stack(s, expm1l(a));
				} else if (!strcmp(buf, "log2")) {	/* log_2 */
					long double a = pop_stack(s);
					push_stack(s, log2l(a));
				} else if (!strcmp(buf, "ln")) {	/* log_e */
					long double a = pop_stack(s);
					push_stack(s, logl(a));
				} else if (!strcmp(buf, "p")) {		/* peek */
					long double a = peek_stack(s);
					printf("%Le\n", a);
				} else if (!strcmp(buf, "P")) {		/* pop */
					long double a = pop_stack(s);
					printf("%Le\n", a);
				} else if (!strcmp(buf, "debug")) {	/* debug */
					for (size_t i = 0; i < s->size; i++)
						printf("\t%zu: %Lf\n", i, s->stack[i]);
					if (s->size == 0)
						printf("\tempty\n");
				} else if (!strcmp(buf, "q")) {		/* quit */
					free(buf);
					return true;
				} else if (strlen(buf) == 2 && buf[0] == 's') { /* store */
					char n = buf[1];
					long double v = peek_stack(s);
					vars[n - 'A'] = v;
				} else if (strlen(buf) == 2 && buf[0] == 'S') { /* store + pop */
					char n = buf[1];
					long double v = pop_stack(s);
					vars[n - 'A'] = v;
				} else if (strlen(buf) == 2 && buf[0] == '=') { /* push var */
					long double v = vars[buf[1] - 'A'];
					push_stack(s, v);
				}
			} else {
				push_stack(s, o);
			}
			memset(buf, 0, read);
			read = 0;

			if (c == EOF)
				break;

			if (c == '\n' && f == stdin)
				fprintf(stderr, "%s", prompt);
			continue;
		}

		buf[read] = c;
		read++;

		if (read == len) {
			len += len>>1;
			char *b = realloc(buf, len);
			if (b == NULL) {
				fprintf(stderr, "Could not allocate more memory!\n");
				free(buf);
				return false;
			}
			buf = b;
			for (size_t i = read; i < len; ++i) {
				buf[i] = '\0';
			}
		}
	}

	free(buf);
	return true;
}

void
init_vars(long double *vars)
{
	memset(vars, 0, 52);

	vars['e' - 'A'] = M_El;
	vars['p' - 'A'] = M_PIl;
}

int
main(int argc, char **argv)
{
	char *env_prompt = getenv("PS2");
	char *prompt = strdup(env_prompt == NULL ? "> " : env_prompt);
	if (prompt == NULL) {
		fprintf(stderr, "Could not allocate memory!\n");
		return EX_UNAVAILABLE;
	}

	long double vars['z' - 'A'];
	init_vars(vars);

	struct stack *s = create_stack(STACK_START_SIZE);
	if (s == NULL) {
		fprintf(stderr, "Could not allocate memory!\n");
		free(prompt);
		return EX_UNAVAILABLE;
	}

	bool sourced_file = false;

	int c;
	while ((c = getopt(argc, argv, "hd:F:f:p:")) != -1) {
		switch (c) {
		case 'p':
			free(prompt);
			prompt = strdup(optarg);
			if (prompt == NULL) {
				fprintf(stderr, "Could not allocate memory!\n");
				destroy_stack(s);
				return EX_UNAVAILABLE;
			}
			break;
		case 'd':
			if (strlen(optarg) < 2) {
				destroy_stack(s);
				free(prompt);
				usage(argv[0]);
				return EX_USAGE;
			}
			char n = optarg[0];
			char *nptr = optarg[1] == '=' ? optarg+2 : optarg+1;
			errno = 0;
			char *end = NULL;
			long double v = strtold(nptr, &end);
			if (errno != 0 || end == nptr) {
				fprintf(stderr, "\"%s\" not a valid number, assuming 0\n", optarg);
				v = 0;
			}
			vars[n - 'A'] = v;
			break;

		case 'F': /* will fall through to case 'f' */
			init_vars(vars);
			destroy_stack(s);
			s = create_stack(STACK_START_SIZE);
			if (s == NULL) {
				fprintf(stderr, "Cannot allocate memory!\n");
				destroy_stack(s);
				free(prompt);
				return EX_UNAVAILABLE;
			}
		case 'f': {
			sourced_file = true;

			FILE *f;
			if (!strcmp(optarg, "-")) {
				f = stdin;
			} else {
				f = fopen(optarg, "r");
			}

			if (f == NULL) {
				perror("Cannot not open file");
				destroy_stack(s);
				free(prompt);
				return EX_UNAVAILABLE;
			}

			bool status = exec_file(f, vars, prompt, s);
			fclose(f);
			if (!status) {
				fprintf(stderr, "Could not allocate memory!\n");
				destroy_stack(s);
				free(prompt);
				return EX_UNAVAILABLE;
			}
			break;
		}

		case 'h':
			usage(argv[0]);
			destroy_stack(s);
			free(prompt);
			return 0;

		default:
			usage(argv[0]);
			destroy_stack(s);
			free(prompt);
			return EX_USAGE;
		}
	}

	bool status = true;
	if (!sourced_file)
		status = exec_file(stdin, vars, prompt, s);

	if (!status) {
		fprintf(stderr, "Could not allocate memory!\n");
		destroy_stack(s);
		free(prompt);
		return EX_UNAVAILABLE;
	}

	destroy_stack(s);
	free(prompt);

	return 0;
}
